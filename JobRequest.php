<?php
Class JobRequest {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result = array();
        try{
            $sql ="Select job_request.*, customer.*, stages 
            from job_request Inner JOIN customer ON customer.cust_id = job_request.cust_id
            left join stages on stages.stage_id = job_request.stage_id ";
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key =$value ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
            //echo 
            $sql .= " order by 1 desc";

            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function getRequests($data=array())
    {
        $result = array();
        try{
            $sql ="Select job_request.*, customer.*, stages
            from client_request job_request inner JOIN customer ON customer.cust_id = job_request.cust_id
            left join stages on stages.stage_id = job_request.stage_id";
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key = $value ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
            //echo $sql;

            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function pending($data=array())
    {
        //Return Variable Array
        $result =array();
        $UserID =$_SESSION["userId"];
        
        try{
            //Get all Data
            $data["job_request.stage_id"] = "getstageid($UserID)";
            $data = $this->getRequests($data);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }

    public function pendingByJob($data=array())
    {
        //Return Variable Array
        $result =array();
        $UserID =$_SESSION["userId"];
        
        try{
            //Get all Data
            $data["job_request.stage_id"] = "getstageid($UserID)";
            $data = $this->getList($data);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }


    public function all($data=array())
    {
        //Return Variable Array
        
        $result =array();
        try{
            //Get all Data
            $data = $this->getRequests($data);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
	
	public function jobs($data=array())
    {
        //Return Variable Array
        
        $result =array();
        try{
            //Get all Data
            $data = $this->getList($data);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }

    public function allItems($data=array())
    {
        //Return Variable Array
        
        $result =array();
        try{
            //Get all Data
            $data = $this->getList($data);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =$_SESSION["userId"];
            //Insert Query
            $sql ="Insert into client_request(cust_id,date_posted,posted_by,job_status_id) Values(?,current_timestamp(),?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->cust_id,$UserID,@$data->job_status_id]);
            $request_id = $db->lastInsertId();
            $data1 = $data->detail;
            $cust_id = $data->cust_id;
            $sendToEstimator = $data->sendToEstimator;
            
            foreach($data1 as $key => $data)
            {
                $sql ="Insert into job_request(cust_id,job_type,fileType,qty,size,pages,binding,paper_det,gsm,cover,txtprint,covprint,lam_type,lam_pos,uv,other_opps,cut_style,packing,add_req,dispatch,del_date,pay_terms,note,request_id,stage_id,other_binding,other_size,date_posted,file_type_id,binding_id,inner_paper_id,cover_paper_id,cover_printing_id,text_printing_id,paper_size_id,gsm_id,transport_id,lamination_position_id,lamination_type_id,packing_id,uv_id) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp(),?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $stmt = $db->prepare($sql);
                //Parameter Placeholder Assigment
                $stmt->execute([@$cust_id,@$data->job_type,@$data->fileType,@$data->qty,@$data->size,@$data->pages,@$data->binding,@$data->paper_det,@$data->gsm,@$data->cover,@$data->txtprint,@$data->covprint,@$data->lam_type,@$data->lam_pos,@$data->uv,@$data->other_opps,@$data->cut_style,@$data->packing,@$data->add_req,@$data->dispatch,@$data->del_date,@$data->pay_terms,@$data->note,@$request_id,@$data->stage_id,@$data->other_binding,@$data->other_size,@$data->file_type_id,@$data->binding_id,@$data->inner_paper_id,@$data->cover_paper_id,@$data->cover_printing_id,@$data->text_printing_id,@$data->paper_size_id,@$data->gsm_id,@$data->transport_id,@$data->lamination_position_id,@$data->lamination_type_id,@$data->packing_id,@$data->uv_id]);
                
                $job_request_id = $db->lastInsertId();

                $sql ="Insert into job_request_finishing(finishing_type_id,job_request_id) Values(?,?)";
                $stmt = $db->prepare($sql);
                $data2 = @$data->finishing_type_id;
                //Parameter Placeholder Assigment
                foreach ($data2 as $key => $item) {
                    $stmt->execute([@$item->finishing_type_id,@$job_request_id]);
                }
            }
            //Get Updated Records
            $message ="";
            if($sendToEstimator == 1)
            {
                $sql ="update job_request set stage_id = ? where request_id =?";
                $stmt = $db->prepare($sql);
                $stmt->execute([2,$request_id]);
                $sql ="update client_request set stage_id = ? where request_id =?";
                $stmt = $db->prepare($sql);
                $stmt->execute([2,$request_id]);
                $message = " and Forwarded to Estimator";
            }
            
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Request Successfully Created $message", "data"=>$data); 
            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =$_SESSION["userId"];
            //Insert Query
            $sql ="Update client_request Set cust_id=?,job_status_id=? Where request_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$data->cust_id,$data->job_status_id,$data->request_id]);
            $request_id = $data->request_id;
            $sendToEstimator = $data->sendToEstimator;
            
            foreach ($data->detail as $key => $data) {
                if(@$data->id>0)
                {
                    $currentValues[]=$data->id;
                }   
            }
            //Delete all not Existent InvoiceItemID
            if(count($currentValues)>0)
            {
                $sql = "Delete from job_request where request_id = ? and id not in (".implode(",", $currentValues).")";
                $stmt = $db->prepare($sql);
                $stmt->execute([$job_request_id]);
            }
            $job_request_id = "";
            foreach($data->detail as $key => $data )
            {
            
                if(@$data->id>0)
                {
                    $sql ="Update job_request Set job_type=?,fileType=?,qty=?,size=?,pages=?,binding=?,paper_det=?,gsm=?,cover=?,txtprint=?,covprint=?,lam_type=?,lam_pos=?,uv=?,other_opps=?,cut_style=?,packing=?,add_req=?,dispatch=?,del_date=?,pay_terms=?,note=?,request_id=?,stage_id=?,other_binding=?,other_size=?,file_type_id=?,binding_id=?,inner_paper_id=?,cover_paper_id=?,cover_printing_id=?,text_printing_id=?,paper_size_id=?,gsm_id=?,transport_id=?,lamination_position_id=?,lamination_type_id=?,packing_id=?,uv_id=? Where id=?";
                   $stmt = $db->prepare($sql);
                   $stmt->execute([$data->job_type,$data->fileType,$data->qty,$data->size,$data->pages,$data->binding,$data->paper_det,$data->gsm,$data->cover,$data->txtprint,$data->covprint,$data->lam_type,$data->lam_pos,$data->uv,$data->other_opps,$data->cut_style,$data->packing,$data->add_req,$data->dispatch,$data->del_date,$data->pay_terms,$data->note,$data->request_id,$data->stage_id,$data->other_binding,$data->other_size,$data->file_type_id,$data->binding_id,$data->inner_paper_id,$data->cover_paper_id,$data->cover_printing_id,$data->text_printing_id,$data->paper_size_id,$data->gsm_id,$data->transport_id,$data->lamination_position_id,$data->lamination_type_id,$data->packing_id,$data->uv_id,$data->id]);
                   //Get Updated Records
                   $job_request_id = $data->id;
                }
                else
                {
                    $sql ="Insert into job_request_finishing(finishing_type_id,job_request_id) Values(?,?)";
                    $stmt = $db->prepare($sql);
                    $stmt->execute([@$data->finishing_type_id,@$request_id]);
                    $job_request_id = $db->lastInsertId();;
                }
                
                //Parameter Placeholder Assigment
                
                $data1 = $data->finishing_type_id;
                $currentValues = array();

                //Find all exiting InvoiceItemID
                foreach ($data1 as $key => $data) {
                    if(@$data->id>0)
                    {
                        $currentValues[]=$data->id;
                    }   
                }
                //Delete all not Existent InvoiceItemID
                if(count($currentValues)>0)
                {
                    $sql = "Delete from job_request_finishing where job_request_id = ? and id not in (".implode(",", $currentValues).")";
                    $stmt = $db->prepare($sql);
                    $stmt->execute([$job_request_id]);
                }
                //Parameter Placeholder Assigment
                foreach($data1 as $key => $data) {
                    if(@$data->id>0)
                    {
                        $sql ="Update job_request_finishing Set finishing_type_id=?,job_request_id=? Where id=?";
                        $stmt = $db->prepare($sql);
                        //Parameter Placeholder Assigment
                        $stmt->execute([$data->finishing_type_id,$data->job_request_id,$data->id]);
                    }
                    else
                    {
                        $sql ="Insert into job_request_finishing(finishing_type_id,job_request_id) Values(?,?)";
                        $stmt = $db->prepare($sql);
                        $stmt->execute([@$data->finishing_type_id,@$job_request_id]);
                    }                
                }
            }
        
            $message ="";
            if($sendToEstimator == 1)
            {
                $sql ="update job_request set stage_id = ? where request_id =?";
                $stmt = $db->prepare($sql);
                $stmt->execute([2,$request_id]);
                $message = " and Forwarded to Estimator";

                $sql ="update client_request set stage_id = ? where request_id =?";
                $stmt = $db->prepare($sql);
                $stmt->execute([2,$request_id]);
                $message = " and Forwarded to Estimator";
            }

            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Request Successfully Updated $message", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="Select job_request.* from client_request job_request 
                INNER JOIN customer ON customer.cust_id = job_request.cust_id where id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
