<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->group("/approvals", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Approvals($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Approvals($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Approvals($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Approvals($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/bindings", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Bindings($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Bindings($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Bindings($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Bindings($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/category", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Category($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Category($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Category($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Category($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/client_request", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ClientRequest($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ClientRequest($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ClientRequest($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ClientRequest($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/cover_papers", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new CoverPapers($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new CoverPapers($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new CoverPapers($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new CoverPapers($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/cover_printing", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new CoverPrinting($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new CoverPrinting($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new CoverPrinting($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new CoverPrinting($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/customer", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Customer($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Customer($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Customer($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Customer($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/dispatch_batches", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new DispatchBatches($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new DispatchBatches($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new DispatchBatches($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new DispatchBatches($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/dispatch_transportation", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new DispatchTransportation($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new DispatchTransportation($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new DispatchTransportation($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new DispatchTransportation($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});
$app->group("/dispatch_unit", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new DispatchUnit($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new DispatchUnit($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new DispatchUnit($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new DispatchUnit($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/equipments", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Equipments($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Equipments($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Equipments($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Equipments($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/expense_item_category", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ExpenseItemCategory($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ExpenseItemCategory($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ExpenseItemCategory($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ExpenseItemCategory($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/expense_item_categoy", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ExpenseItemCategoy($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ExpenseItemCategoy($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ExpenseItemCategoy($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ExpenseItemCategoy($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/expense_items", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ExpenseItems($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ExpenseItems($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ExpenseItems($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ExpenseItems($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/file_category", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileCategory($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileCategory($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new FileCategory($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new FileCategory($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/file_group", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileGroup($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileGroup($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new FileGroup($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new FileGroup($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/file_group_staff", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileGroupStaff($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileGroupStaff($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new FileGroupStaff($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new FileGroupStaff($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/file_group_upload", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileGroupUpload($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileGroupUpload($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new FileGroupUpload($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new FileGroupUpload($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/file_types", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileTypes($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FileTypes($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new FileTypes($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new FileTypes($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/finishing_type", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FinishingType($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FinishingType($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new FinishingType($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new FinishingType($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/inner_paper", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new InnerPaper($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new InnerPaper($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new InnerPaper($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new InnerPaper($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Items($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Items($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Items($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Items($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_brand", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsBrand($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsBrand($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsBrand($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsBrand($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_color", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsColor($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsColor($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsColor($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsColor($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_grade", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsGrade($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsGrade($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsGrade($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsGrade($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_gsm", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsGsm($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsGsm($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsGsm($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsGsm($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_info", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsInfo($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsInfo($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsInfo($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsInfo($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_store", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsStore($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsStore($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsStore($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsStore($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_type", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsType($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsType($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsType($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsType($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_unit", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsUnit($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsUnit($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsUnit($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsUnit($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/items_unitgroup", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsUnitgroup($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ItemsUnitgroup($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ItemsUnitgroup($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ItemsUnitgroup($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/job_card", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobCard($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobCard($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new JobCard($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new JobCard($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/job_request", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobRequest($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobRequest($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new JobRequest($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/jobs", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new JobRequest($this->db);
        $result = $table->jobs(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/list", function(Request $request, Response $response, array $args){
         $data = json_decode($request->getBody());
            $table = new JobRequest($this->db);
            $result = $table->allItems($data);
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
        });

    $app->get("/estimator", function(Request $request, Response $response, array $args){
        // $data = json_decode($request->getBody());
            $table = new JobRequest($this->db);
            $result = $table->all(array("job_request.stage_id"=>2));
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
    });

    $app->get("/pending", function(Request $request, Response $response, array $args){
        // $data = json_decode($request->getBody());
            $table = new JobRequest($this->db);
            $result = $table->pending(array());
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
    });

    $app->get("/pending-job", function(Request $request, Response $response, array $args){
        // $data = json_decode($request->getBody());
            $table = new JobRequest($this->db);
            $result = $table->pendingByJob(array());
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
    });
    
    $app->get("/front_desk", function(Request $request, Response $response, array $args){
        // $data = json_decode($request->getBody());
            $table = new JobRequest($this->db);
            $result = $table->all(array("stage_id"=>1));
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
        });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new JobRequest($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});//->add($auth);

$app->group("/job_request_finishing", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobRequestFinishing($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobRequestFinishing($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new JobRequestFinishing($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new JobRequestFinishing($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/job_schedule", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobSchedule($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobSchedule($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new JobSchedule($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new JobSchedule($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/job_schedule_details", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobScheduleDetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new JobScheduleDetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new JobScheduleDetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new JobScheduleDetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/lamination_position", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LaminationPosition($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LaminationPosition($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new LaminationPosition($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new LaminationPosition($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/lamination_types", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LaminationTypes($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LaminationTypes($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new LaminationTypes($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new LaminationTypes($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/machine_operations", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new MachineOperations($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new MachineOperations($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new MachineOperations($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new MachineOperations($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/packing", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Packing($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Packing($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Packing($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Packing($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/paper_sizes", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PaperSizes($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PaperSizes($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PaperSizes($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PaperSizes($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/payment", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Payment($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Payment($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Payment($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Payment($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/getTotal/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Payment($this->db);
        $result = $table->getTotal($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/listReceipt", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $table = new Payment($this->db);
        $result = $table->viewReceipts();
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/getReceipt/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Payment($this->db);
        $result = $table->getReceipt($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/payment_method", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PaymentMethod($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PaymentMethod($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PaymentMethod($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PaymentMethod($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/payment_type", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PaymentType($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PaymentType($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PaymentType($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PaymentType($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/postpress", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Postpress($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Postpress($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Postpress($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Postpress($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/postpress_activities", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PostpressActivities($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PostpressActivities($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PostpressActivities($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PostpressActivities($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/prepress", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Prepress($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Prepress($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Prepress($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->post("/list", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Prepress($this->db);
        $result = $table->all($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Prepress($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/prepress2", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Prepress2($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Prepress2($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Prepress2($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Prepress2($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/press", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Press($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Press($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Press($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Press($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/press_test", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PressTest($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PressTest($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PressTest($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PressTest($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/press_test_activity", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PressTestActivity($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PressTestActivity($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PressTestActivity($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PressTestActivity($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/press_test_results", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PressTestResults($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PressTestResults($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PressTestResults($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PressTestResults($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/privilege_activity", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeActivity($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeActivity($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PrivilegeActivity($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PrivilegeActivity($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/privilege_activitydetails", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeActivitydetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeActivitydetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PrivilegeActivitydetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PrivilegeActivitydetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/privilege_class", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeClass($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeClass($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PrivilegeClass($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PrivilegeClass($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/privilege_details", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeDetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeDetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PrivilegeDetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PrivilegeDetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/privilege_header", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeHeader($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeHeader($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PrivilegeHeader($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PrivilegeHeader($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/privilege_type", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeType($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeType($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PrivilegeType($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PrivilegeType($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/privileges", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privileges($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privileges($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Privileges($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Privileges($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/product", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Product($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Product($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Product($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Product($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/production_log", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ProductionLog($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ProductionLog($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ProductionLog($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ProductionLog($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/production_log_history", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ProductionLogHistory($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ProductionLogHistory($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ProductionLogHistory($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ProductionLogHistory($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/production_log_job", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ProductionLogJob($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ProductionLogJob($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ProductionLogJob($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ProductionLogJob($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/requisition", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Requisition($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Requisition($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Requisition($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Requisition($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/requisition_approvals", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new RequisitionApprovals($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new RequisitionApprovals($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new RequisitionApprovals($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new RequisitionApprovals($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/requisition_detail", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new RequisitionDetail($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new RequisitionDetail($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new RequisitionDetail($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new RequisitionDetail($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/requisition_reasons", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new RequisitionReasons($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new RequisitionReasons($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new RequisitionReasons($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new RequisitionReasons($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/sales", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Sales($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Sales($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Sales($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Sales($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get-invoice/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Sales($this->db);
        $result = $table->getByRequest($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get-full-invoice/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Sales($this->db);
        $result = $table->getFullRequest($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/sales_details", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new SalesDetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new SalesDetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new SalesDetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new SalesDetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/services", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Services($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Services($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Services($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Services($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/services_cat", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ServicesCat($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ServicesCat($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ServicesCat($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ServicesCat($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/shift_types", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ShiftTypes($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ShiftTypes($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ShiftTypes($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ShiftTypes($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/shopping_cart", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ShoppingCart($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ShoppingCart($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ShoppingCart($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get-cart/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ShoppingCart($this->db);
        $result = $table->getbyRequest($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ShoppingCart($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });


});

$app->group("/staff", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Staff($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Staff($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Staff($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Staff($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/stage_flow", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new StageFlow($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new StageFlow($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new StageFlow($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new StageFlow($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/stages", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Stages($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Stages($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Stages($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Stages($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/get-by-request/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Stages($this->db);
        $result = $table->getbyRequest($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/stages_group", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new StagesGroup($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new StagesGroup($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new StagesGroup($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new StagesGroup($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/stockin", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Stockin($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Stockin($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Stockin($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Stockin($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/sub_stages", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new SubStages($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new SubStages($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new SubStages($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new SubStages($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/supplier", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Supplier($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Supplier($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Supplier($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Supplier($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/tbl_menu", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TblMenu($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TblMenu($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new TblMenu($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new TblMenu($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/tbl_menuauthorise", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TblMenuauthorise($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TblMenuauthorise($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new TblMenuauthorise($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new TblMenuauthorise($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/text_printing", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TextPrinting($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TextPrinting($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new TextPrinting($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new TextPrinting($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/units", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Units($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Units($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Units($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Units($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/user", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new User($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new User($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new User($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new User($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/usergroup", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Usergroup($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Usergroup($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Usergroup($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Usergroup($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/uv_exposure", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new UvExposure($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new UvExposure($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new UvExposure($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new UvExposure($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/vat", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Vat($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Vat($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Vat($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Vat($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/voucher", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Voucher($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Voucher($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Voucher($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/vendors", function(Request $request, Response $response, array $args){
        // $data = json_decode($request->getBody());
            $table = new Voucher($this->db);
            $result = $table->vendors();
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
        });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Voucher($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/voucher_details", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new VoucherDetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new VoucherDetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new VoucherDetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new VoucherDetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/vprivilegedetails", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Vprivilegedetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Vprivilegedetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Vprivilegedetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Vprivilegedetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

$app->group("/wht", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Wht($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Wht($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Wht($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Wht($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

});

