<?php
Class Resultsummary {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="Select resultsummary.*, resultsummary.resultsummary_id as id, classdegree.class_degree,courses.course_title,levels.level,s_sessionconfig.isActive,student.level_id from resultsummary left join classdegree on resultsummary.classdegree_id = classdegree.classdegree_id  left join courses on resultsummary.course_id = courses.course_id  left join levels on resultsummary.level_id = levels.level_id  left join s_sessionconfig on resultsummary.s_sessionconfig_id = s_sessionconfig.s_sessionconfig_id  left join student on resultsummary.student_id = student.student_id ";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }
    public function GenerateResult($dataPosted)
    {
        $result =array();
        try{
            $UserID =$_SESSION["userId"];
            $db = $this->db;
            $sql ="call udp_generateresultsummary(?,?,?)";
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([$dataPosted->s_sessionconfig_id, $dataPosted->program_id, $dataPosted->level_id]);
            
            $sql ="Select resultsummary.*, coursestudy_name, resultsummary.resultsummary_id as id, classdegree.class_degree,levels.level,s_sessionconfig.isActive,student.firstname, surname, othername, reg_no from resultsummary 
            left join classdegree on resultsummary.classdegree_id = classdegree.classdegree_id  
            left join courses on resultsummary.course_id = courses.course_id  
            left join levels on resultsummary.level_id = levels.level_id  
            left join s_sessionconfig on resultsummary.s_sessionconfig_id = s_sessionconfig.s_sessionconfig_id  
            left join student on resultsummary.student_id = student.student_id  
            left join coursestudy on coursestudy.coursestudy_id = resultsummary.coursestudy_id  
            where resultsummary.s_sessionconfig_id = ? and resultsummary.coursestudy_id = ? and resultsummary.level_id=? ";
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([$dataPosted->s_sessionconfig_id, $dataPosted->program_id, $dataPosted->level_id]);
            $data2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $sql ="Call udp_allstudentresult(?,?,?)";
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([$dataPosted->s_sessionconfig_id, $dataPosted->level_id, $dataPosted->program_id]);
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        
            $sql ="Select * from appsettings";
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute();
            $school = $stmt->fetch(PDO::FETCH_ASSOC);

            $sql ="Select firstname, surname, othername, signature from college_heads school_head left join hr_staff on hr_staff.staff_id= school_head.staff_id where s_sessionconfig_id=?";
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([$dataPosted->s_sessionconfig_id]);
            $school_head = $stmt->fetch(PDO::FETCH_ASSOC);

            //$sql ="Select session_name, semester_name from s_sessionconfig left join where s_sessionconfig_id=?";
            $sql ="Select semester.semester_name,sessions.session_name from s_sessionconfig left join semester on s_sessionconfig.semester_id = semester.semester_id  left join sessions on s_sessionconfig.session_id = sessions.session_id  where s_sessionconfig_id=?";
            
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([$dataPosted->s_sessionconfig_id]);
            $session_data = $stmt->fetch(PDO::FETCH_ASSOC);

            
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=> array("result"=> $data2, "detail"=> $data, 'school'=>$school, 'school_head'=>$school_head, 'session_data'=>$session_data )); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into resultsummary(date_posted,remark,avg_score,total,posted_by,posted_date,last_modify_date,last_modify_by,classdegree_id,level_id,course_id,student_id,s_sessionconfig_id) Values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->date_posted,@$data->remark,@$data->avg_score,@$data->total,@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->last_modify_by,@$data->classdegree_id,@$data->level_id,@$data->course_id,@$data->student_id,@$data->s_sessionconfig_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update resultsummary Set date_posted=?,remark=?,avg_score=?,total=?,posted_by=?,posted_date=?,last_modify_date=?,last_modify_by=?,classdegree_id=?,level_id=?,course_id=?,student_id=?,s_sessionconfig_id=? Where resultsummary_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->date_posted,@$data->remark,@$data->avg_score,@$data->total,@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->last_modify_by,@$data->classdegree_id,@$data->level_id,@$data->course_id,@$data->student_id,@$data->s_sessionconfig_id,$data->resultsummary_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="Select * from resultsummary where resultsummary_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
