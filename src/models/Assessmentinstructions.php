<?php
Class Assessmentinstructions {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="Select assessmentinstructions.*, assessmentinstructions.assessmentinstructions_id as id, assessmentgroup.assessmentgroup_name,courses.course_title,s_sessionconfig.isActive from assessmentinstructions left join assessmentgroup on assessmentinstructions.assessmentgroup_id = assessmentgroup.assessmentgroup_id  left join courses on assessmentinstructions.course_id = courses.course_id  left join s_sessionconfig on assessmentinstructions.s_sessionconfig_id = s_sessionconfig.s_sessionconfig_id ";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    
    public function LeturesAssigned($data=array())
    {
        //Return Variable Array
        $result =array();
        $UserID =$_SESSION["userId"];

        try{
            $sql ="SELECT assessmentinstructions.*,  assessmentinstructions.assessmentinstructions_id AS id, assessmentinstructions_name, assessmentgroup.assessmentgroup_name FROM 
            assessmentinstructions 
            LEFT JOIN assessmentgroup ON assessmentinstructions.assessmentgroup_id = assessmentgroup.assessmentgroup_id 
             WHERE course_id IN (SELECT course_id FROM lectures WHERE staff_id =? and s_sessionconfig_id IN (SELECT s_sessionconfig_id FROM s_sessionconfig WHERE isActive=1))
             AND  assessmentinstructions.s_sessionconfig_id IN (SELECT s_sessionconfig_id FROM s_sessionconfig WHERE isActive=1)
            ";
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$UserID]);
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //Get all Data
            
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into assessmentinstructions(assessmentgroup_id,assessmentinstructions_name,abbr,score,posted_by,posted_date,last_modify_date,last_modify_by,active_id,cloned_id,course_id,s_sessionconfig_id) Values(?,?,?,?,?,?,?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->assessmentgroup_id,@$data->assessmentinstructions_name,@$data->abbr,@$data->score,@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->last_modify_by,@$data->active_id,@$data->cloned_id,@$data->course_id,@$data->s_sessionconfig_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update assessmentinstructions Set assessmentgroup_id=?,assessmentinstructions_name=?,abbr=?,score=?,posted_by=?,posted_date=?,last_modify_date=?,last_modify_by=?,active_id=?,cloned_id=?,course_id=?,s_sessionconfig_id=? Where assessmentinstructions_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->assessmentgroup_id,@$data->assessmentinstructions_name,@$data->abbr,@$data->score,@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->last_modify_by,@$data->active_id,@$data->cloned_id,@$data->course_id,@$data->s_sessionconfig_id,$data->assessmentinstructions_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="Select * from assessmentinstructions where assessmentinstructions_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
