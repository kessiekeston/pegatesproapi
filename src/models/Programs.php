<?php
Class Programs {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="select programs.*, programs.program_id AS id, programcategory.programcategory_name, departments.`dept_name`, departments.`dept_code`,
            departments.`description`, faculty.`faculty_name` FROM programs 
            LEFT JOIN programcategory ON programs.programcategory_id = programcategory.programcategory_id
            LEFT JOIN departments ON programs.`dept_id` = departments.`dept_id`
            LEFT JOIN faculty ON departments.`faculty_id` = faculty.`faculty_id`";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into programs(program_name,programcategory_id, dept_id,posted_by,last_modify_date,last_modify_by) Values(?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->program_name,@$data->programcategory_id, @$data->dept_id,@$data->posted_by,@$data->last_modify_date,@$data->last_modify_by]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update programs Set program_name=?,programcategory_id=?, dept_id = ?,posted_by=?,last_modify_date=?,last_modify_by=? Where program_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->program_name,@$data->programcategory_id, @$data->dept_id,@$data->posted_by,@$data->last_modify_date,@$data->last_modify_by,$data->program_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="select programs.*, programs.program_id AS id, programcategory.programcategory_name, departments.`dept_id`,departments.`dept_name`, departments.`dept_code`,
            departments.`description`, faculty.`faculty_id`,faculty.`faculty_name` FROM programs 
            LEFT JOIN programcategory ON programs.programcategory_id = programcategory.programcategory_id
            LEFT JOIN departments ON programs.`dept_id` = departments.`dept_id`
            LEFT JOIN faculty ON departments.`faculty_id` = faculty.`faculty_id`
            WHERE program_id=3";

            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
