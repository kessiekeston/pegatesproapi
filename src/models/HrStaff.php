<?php
Class HrStaff {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="SELECT CONCAT(hr_staff.surname, ' ', hr_staff.firstname, ' ', IFNULL(hr_staff.othername, '')) AS staff_name, 
            hr_staff.*, gender.`gender`, maritalstatus.`maritalstatus`, hr_staff.staff_id AS id, country.country_id,faculty.faculty_name,hr_banks.bank_name,hr_department.dept_name,
            hr_designation.designation_name,hr_employeetype.employeetype,local_govt.local_govt,state.state,statuses.status_name FROM hr_staff 
            LEFT JOIN country ON hr_staff.country_id = country.country_id  
            LEFT JOIN faculty ON hr_staff.faculty_id = faculty.faculty_id  
            LEFT JOIN hr_banks ON hr_staff.bank_id = hr_banks.bank_id  
            LEFT JOIN hr_department ON hr_staff.department_id = hr_department.department_id  
            LEFT JOIN hr_designation ON hr_staff.designation_id = hr_designation.designation_id  
            LEFT JOIN hr_employeetype ON hr_staff.employeetype_id = hr_employeetype.employeetype_id  
            LEFT JOIN local_govt ON hr_staff.local_govt_id = local_govt.local_govt_id  
            LEFT JOIN state ON hr_staff.state_id = state.state_id  
            LEFT JOIN statuses ON hr_staff.status_id = statuses.status_id 
            LEFT JOIN gender ON hr_staff.`gender_id` = gender.`gender_id`
            LEFT JOIN maritalstatus ON hr_staff.`maritalStatus_id` = maritalstatus.`maritalstatus_id`";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into hr_staff(department_id,staff_code,surname,firstname,othername,email,phone,passport,gender_id,dob,maritalStatus_id,address,title,posted_by,signature,designation_id,faculty_id,bank_id,employeetype_id,status_id,country_id,state_id,local_govt_id) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->department_id,@$data->staff_code,@$data->surname,@$data->firstname,@$data->othername,@$data->email,@$data->phone,@$data->passport,@$data->gender_id,@$data->dob,@$data->maritalStatus_id,@$data->address,@$data->title,@$data->posted_by,@$data->signature,@$data->designation_id,@$data->faculty_id,@$data->bank_id,@$data->employeetype_id,@$data->status_id,@$data->country_id,@$data->state_id,@$data->local_govt_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update hr_staff Set department_id=?,staff_code=?,surname=?,firstname=?,othername=?,email=?,phone=?,passport=?,gender_id=?,dob=?,maritalStatus_id=?,address=?,title=?,posted_by=?,signature=?,designation_id=?,faculty_id=?,bank_id=?,employeetype_id=?,status_id=?,country_id=?,state_id=?,local_govt_id=? Where staff_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->department_id,@$data->staff_code,@$data->surname,@$data->firstname,@$data->othername,@$data->email,@$data->phone,@$data->passport,@$data->gender_id,@$data->dob,@$data->maritalStatus_id,@$data->address,@$data->title,@$data->posted_by,@$data->signature,@$data->designation_id,@$data->faculty_id,@$data->bank_id,@$data->employeetype_id,@$data->status_id,@$data->country_id,@$data->state_id,@$data->local_govt_id,$data->staff_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="Select * from hr_staff where staff_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
	
	public function generateStaffCode()
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="select ifNULL(max(staff_id),0)+1 as staff_code from hr_staff";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
			if($data['staff_code']<10)
			{
				$data['staff_code'] = "00".$data['staff_code'];
			}
			else if($data['staff_code']<100)
			{
				$data['staff_code'] = "0".$data['staff_code'];
			}
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
