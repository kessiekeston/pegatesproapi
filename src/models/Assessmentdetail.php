<?php
Class Assessmentdetail {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="Select assessmentdetail.*, assessmentdetail.assessmentdetaill_id as id, assessmentinstructions.assessmentgroup_id,courses.course_title,levels.level,student.level_id from assessmentdetail left join assessmentinstructions on assessmentdetail.assessmentinstructions_id = assessmentinstructions.assessmentinstructions_id  left join courses on assessmentdetail.course_id = courses.course_id  left join levels on assessmentdetail.level_id = levels.level_id  left join s_sessionconfig on assessmentdetail.s_sessionconfig_id = s_sessionconfig.s_sessionconfig_id  left join student on assessmentdetail.student_id = student.student_id ";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into assessmentdetail(student_id,score,date_posted,posted_by,last_modify_date,last_modify_by,course_id,level_id,assessmentinstructions_id,s_sessionconfig_id) Values(?,?,?,?,?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->student_id,@$data->score,@$data->date_posted,@$data->posted_by,@$data->last_modify_date,@$data->last_modify_by,@$data->course_id,@$data->level_id,@$data->assessmentinstructions_id,@$data->s_sessionconfig_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }

    public function SaveScores($dataPosted)
    {
        $result =array();
        try{
            $UserID =$_SESSION["userId"];
            //Insert Query
            $sql ="Update assessmentdetail Set score=?,last_modify_date=current_timestamp(),last_modify_by=? Where assessmentdetaill_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
           
            foreach($dataPosted->detail as $data)
            {
                $stmt->execute([@$data->score,$UserID,$data->assessmentdetaill_id]);
            }

            $sql ="call udp_generateassessmentsummary(?)";
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([$dataPosted->lecture_id]);
            
            
            $result = array("status"=> 0, "message"=> "Record Successfully Updated"); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }

    public function StudentByInstructions($data)
    {
        $result =array();
        try{
            $UserID =0;
            $db = $this->db;
            //Insert Query

            $sql ="call udp_insertAssessmentDetail(?,?)";
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([$data->assessmentinstructions_id, $data->lecture_id]);
            
            $sql ="Select assessmentdetail.*, assessmentdetail.assessmentdetaill_id as id, student.reg_no, student.firstname, surname, othername from assessmentdetail left join  student on assessmentdetail.student_id = student.student_id  where assessmentinstructions_id =?";
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->assessmentinstructions_id]);
            //Get Updated Records
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update assessmentdetail Set student_id=?,score=?,date_posted=?,posted_by=?,last_modify_date=?,last_modify_by=?,course_id=?,level_id=?,assessmentinstructions_id=?,s_sessionconfig_id=? Where assessmentdetaill_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->student_id,@$data->score,@$data->date_posted,@$data->posted_by,@$data->last_modify_date,@$data->last_modify_by,@$data->course_id,@$data->level_id,@$data->assessmentinstructions_id,@$data->s_sessionconfig_id,$data->assessmentdetaill_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="Select * from assessmentdetail where assessmentdetaill_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
