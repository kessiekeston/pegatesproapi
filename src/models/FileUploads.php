<?php
require_once './vendor/autoload.php';
use Slim\Http\UploadedFile;

Class FileUploads {
	function moveUploadedFile($directory, UploadedFile $uploadedFile)
	{
		$extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
		$basename = bin2hex($this->randomize(16)); // see http://php.net/manual/en/function.random-bytes.php
		$filename = "FILE_".sprintf('%s.%0.8s', $basename, $extension);
		
		try{
			$uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
			$filePath = $directory."/".$filename;
			$result = array("status"=> 0, "message"=> "File Successfully Uploaded", "data"=>trim($filePath, './'));     
//print_r($result); die;			
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
		return $result;
	}
	
	function randomize($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') 
	{
		for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
		return $s;
	}

}
