<?php
Class Student {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="select student.*, student.student_id AS id, country.Code,programs.`program_name`, faculty.`faculty_name`,departments.dept_name,
            levels.level,local_govt.local_govt,state.state,student_status.status, gender.`gender` FROM student 
            LEFT JOIN country ON student.country_id = country.country_id  
            LEFT JOIN programs ON student.`program_id` = programs.`program_id`
            LEFT JOIN departments ON student.department_id = departments.`dept_id`
            LEFT JOIN levels ON student.level_id = levels.level_id  
            LEFT JOIN local_govt ON student.local_govt_id = local_govt.local_govt_id  
            LEFT JOIN state ON student.state_id = state.state_id  
            LEFT JOIN student_status ON student.student_statusid = student_status.student_statusid
            LEFT JOIN faculty ON student.`faculty_id` = faculty.`faculty_id`
            LEFT JOIN gender ON student.`gender_id` = gender.`gender_id`";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into student(level_id,reg_no,surname,firstname,othername,student_address,guardian_name,guardian_phone,guardian_address,gender,dob,guardian_email,image_url,entry_year,entry_level_id,active_id,posted_by,posted_date,last_modify_date,last_modify_by,emergency_email,emergency_phone,voter_code,student_statusid,faculty_id,department_id,country_id,state_id,local_govt_id,program_id) Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->level_id,@$data->reg_no,@$data->surname,@$data->firstname,@$data->othername,@$data->student_address,@$data->guardian_name,@$data->guardian_phone,@$data->guardian_address,@$data->gender,@$data->dob,@$data->guardian_email,@$data->image_url,@$data->entry_year,@$data->entry_level_id,@$data->active_id,@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->last_modify_by,@$data->emergency_email,@$data->emergency_phone,@$data->voter_code,@$data->student_statusid,@$data->faculty_id,@$data->department_id,@$data->country_id,@$data->state_id,@$data->local_govt_id,@$data->program_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update student Set level_id=?,reg_no=?,surname=?,firstname=?,othername=?,student_address=?,guardian_name=?,guardian_phone=?,guardian_address=?,gender=?,dob=?,guardian_email=?,image_url=?,entry_year=?,entry_level_id=?,active_id=?,posted_by=?,posted_date=?,last_modify_date=?,last_modify_by=?,emergency_email=?,emergency_phone=?,voter_code=?,student_statusid=?,faculty_id=?,department_id=?,country_id=?,state_id=?,local_govt_id=?,program_id=? Where student_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->level_id,@$data->reg_no,@$data->surname,@$data->firstname,@$data->othername,@$data->student_address,@$data->guardian_name,@$data->guardian_phone,@$data->guardian_address,@$data->gender,@$data->dob,@$data->guardian_email,@$data->image_url,@$data->entry_year,@$data->entry_level_id,@$data->active_id,@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->last_modify_by,@$data->emergency_email,@$data->emergency_phone,@$data->voter_code,@$data->student_statusid,@$data->faculty_id,@$data->department_id,@$data->country_id,@$data->state_id,@$data->local_govt_id,@$data->program_id,$data->student_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="select student.*, student.student_id AS id, country.Code,programs.`program_name`, faculty.`faculty_name`,departments.dept_name,
            levels.level,local_govt.local_govt,state.state,student_status.status, gender.`gender` FROM student 
            LEFT JOIN country ON student.country_id = country.country_id  
            LEFT JOIN programs ON student.`program_id` = programs.`program_id`
            LEFT JOIN departments ON student.department_id = departments.`dept_id`
            LEFT JOIN levels ON student.level_id = levels.level_id  
            LEFT JOIN local_govt ON student.local_govt_id = local_govt.local_govt_id  
            LEFT JOIN state ON student.state_id = state.state_id  
            LEFT JOIN student_status ON student.student_statusid = student_status.student_statusid
            LEFT JOIN faculty ON student.`faculty_id` = faculty.`faculty_id`
            LEFT JOIN gender ON student.`gender_id` = gender.`gender_id` where student_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }

    public function generateStaffCode()
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="select ifNULL(max(student_id),0)+1 as reg_no from student";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
			if($data['reg_no']<10)
			{
				$data['reg_no'] = "00".$data['reg_no'];
			}
			else if($data['reg_no']<100)
			{
				$data['reg_no'] = "0".$data['reg_no'];
			}
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
