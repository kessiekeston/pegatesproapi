<?php
Class LecturerCourseMapping {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="select scm.id, s.`student_id`, s.`firstname`, c.`course_id`, c.`course_title`, c.`course_code`, c.`credit_unit`, ot.`offertype_id`, ot.`offertype`,
            semester.`semester_id`, semester.`semester_name` FROM student_course_mapping scm
            LEFT JOIN student s ON scm.`student_id` = s.`student_id`
            LEFT JOIN courses c ON scm.`course_id` = c.`course_id`
            LEFT JOIN offertype ot ON scm.`offertype_id` = ot.`offertype_id`
            LEFT JOIN semester ON scm.`semester_id` = semester.`semester_id`";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into lecturer_course_mapping (staff_id, faculty_id, dept_id, prog_id, course_id) Values(?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            // foreach($data as $key => $val)
			// 	{
			// 		// $stmt->execute([$val->CollateralID, $val->DocumentSubmittedID, $val->VerificationStatusID]);
            //     }
                $stmt->execute([@$data->staff_id, @$data->faculty_id, @$data->dept_id, @$data->prog_id, @$data->course_id]);

            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query  , , , , 
            $sql ="Update lecturer_course_mapping Set staff_id=?, faculty_id = ?, dept_id = ?, prog_id = ?, course_id = ? Where staff_id=? and course_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment ([]);
            $stmt->execute([@$data->faculty_id, @$data->dept_id, @$data->prog_id, @$data->course_id,$data->id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="select s.`student_id`, s.`firstname`, c.`course_id`, c.`course_title`, c.`course_code`, c.`credit_unit`, ot.`offertype_id`, ot.`offertype`,
            semester.`semester_id`, semester.`semester_name` FROM student_course_mapping scm
            LEFT JOIN student s ON scm.`student_id` = s.`student_id`
            LEFT JOIN courses c ON scm.`course_id` = c.`course_id`
            LEFT JOIN offertype ot ON scm.`offertype_id` = ot.`offertype_id`
            LEFT JOIN semester ON scm.`semester_id` = semester.`semester_id`
            WHERE scm.`student_id` = ?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
