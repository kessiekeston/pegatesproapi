<?php
Class FacDeptProgCourseMapping {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="SELECT id, f.`faculty_id`, f.`faculty_name`, d.`dept_id`, d.`dept_name`, d.`dept_code`, p.`program_id`, p.`program_name`, c.`course_title`, pc.`programcategory_id`, 
            pc.`programcategory_name`, c.`course_id`, c.`course_code`, CONCAT(hrs.surname, ' ', hrs.firstname, ' ', IFNULL(hrs.othername, '')) AS staff_name FROM fac_dept_prog_course_mapping fdpc
                        INNER JOIN faculty f ON fdpc.`faculty_id` = f.`faculty_id`
                        INNER JOIN departments d ON fdpc.`dept_id` = d.`dept_id`
                        INNER JOIN programs p ON fdpc.`prog_id` = p.`program_id`
                        INNER JOIN programcategory pc ON p.`programcategory_id` = pc.`programcategory_id`  
                        INNER JOIN courses c ON fdpc.`course_id` = c.`course_id`
                        LEFT JOIN hr_staff hrs ON fdpc.`staff_id` = hrs.`staff_id`";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into fac_dept_prog_course_mapping (faculty_id, dept_id, prog_id, course_id) Values(?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            foreach($data as $key => $val)
				{
					// $stmt->execute([$val->CollateralID, $val->DocumentSubmittedID, $val->VerificationStatusID]);
                    $stmt->execute([@$val->faculty_id, @$val->dept_id, @$val->prog_id, @$val->course_id]);
                }

            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update fac_dept_prog_course_mapping Set faculty_id=?, dept_id = ?, prog_id = ?, course_id = ?, staff_id = ? Where id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment ([]);
            $stmt->execute([@$data->faculty_id, @$data->dept_id, @$data->prog_id, @$data->course_id, @$data->staff_id,$data->id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    // public function get($id)
    // {
    //     //Return Variable Array
    //     $result =array();
    //     try{
    //         $sql ="Select * from fac_dept_prog_course_mapping where role_id=?";
    //         $db = $this->db;
    //         $stmt = $db->prepare($sql);
    //         $stmt->execute([$id]);
    //         $data = $stmt->fetch(PDO::FETCH_ASSOC);
    //         //Return Variable Assignment (Success)
    //         $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
    //         $db = null; //De-assigned Database Variable
    //     }
    //     catch(PDOException $e) {
    //         //Return Variable Assignment (Error)
    //         $result = array("status"=> 100, "message"=> $e->getMessage());
    //         //Logger    
    //     }
    //     return $result;
    // }
}
