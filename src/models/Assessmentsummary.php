<?php
Class Assessmentsummary {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="Select assessmentsummary.*, assessmentsummary.assessmentsummary_id as id, courses.course_title,levels.level,s_sessionconfig.isActive,student.level_id from assessmentsummary left join courses on assessmentsummary.course_id = courses.course_id  left join levels on assessmentsummary.level_id = levels.level_id  left join s_sessionconfig on assessmentsummary.s_sessionconfig_id = s_sessionconfig.s_sessionconfig_id  left join student on assessmentsummary.student_id = student.student_id ";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into assessmentsummary(student_id,date_posted,grade,total,posted_by,posted_date,last_modify_date,last_modify_by,remark,level_id,course_id,s_sessionconfig_id) Values(?,?,?,?,?,?,?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->student_id,@$data->date_posted,@$data->grade,@$data->total,@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->last_modify_by,@$data->remark,@$data->level_id,@$data->course_id,@$data->s_sessionconfig_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update assessmentsummary Set student_id=?,date_posted=?,grade=?,total=?,posted_by=?,posted_date=?,last_modify_date=?,last_modify_by=?,remark=?,level_id=?,course_id=?,s_sessionconfig_id=? Where assessmentsummary_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->student_id,@$data->date_posted,@$data->grade,@$data->total,@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->last_modify_by,@$data->remark,@$data->level_id,@$data->course_id,@$data->s_sessionconfig_id,$data->assessmentsummary_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="Select * from assessmentsummary where assessmentsummary_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
