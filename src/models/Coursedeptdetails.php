<?php
Class Coursedeptdetails {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="Select coursedeptdetails.*, coursedeptdetails.coursedeptdetails_id as id, courses.course_title,coursestudy.coursestudy_name,hr_department.dept_name,levels.level,offertype.offertype,s_sessionconfig.isActive from coursedeptdetails left join courses on coursedeptdetails.course_id = courses.course_id  left join coursestudy on coursedeptdetails.coursestudy_id = coursestudy.coursestudy_id  left join hr_department on coursedeptdetails.department_id = hr_department.department_id  left join levels on coursedeptdetails.level_id = levels.level_id  left join offertype on coursedeptdetails.offertype_id = offertype.offertype_id  left join s_sessionconfig on coursedeptdetails.s_sessionconfig_id = s_sessionconfig.s_sessionconfig_id ";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Insert into coursedeptdetails(posted_by,posted_date,last_modify_date,credit_unit,last_modify_by,course_id,department_id,offertype_id,coursestudy_id,level_id,s_sessionconfig_id) Values(?,?,?,?,?,?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->credit_unit,@$data->last_modify_by,@$data->course_id,@$data->department_id,@$data->offertype_id,@$data->coursestudy_id,@$data->level_id,@$data->s_sessionconfig_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        $result =array();
        try{
            $UserID =0;
            //Insert Query
            $sql ="Update coursedeptdetails Set posted_by=?,posted_date=?,last_modify_date=?,credit_unit=?,last_modify_by=?,course_id=?,department_id=?,offertype_id=?,coursestudy_id=?,level_id=?,s_sessionconfig_id=? Where coursedeptdetails_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->posted_by,@$data->posted_date,@$data->last_modify_date,@$data->credit_unit,@$data->last_modify_by,@$data->course_id,@$data->department_id,@$data->offertype_id,@$data->coursestudy_id,@$data->level_id,@$data->s_sessionconfig_id,$data->coursedeptdetails_id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="Select * from coursedeptdetails where coursedeptdetails_id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
}
