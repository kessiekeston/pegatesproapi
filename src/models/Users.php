<?php
Class Users {
	private $db;

	public function __construct($connection)
	{
		$this->db= $connection;
	}

    public function getList($data=array())
    {
        $result =array();
        try{
            $sql ="SELECT CONCAT(hr_staff.`surname`,' ', hr_staff.`firstname`, ' ', IFNULL(hr_staff.`othername`, '')) AS staffname, users.*, 
            users.id AS id, role.role_name FROM hr_staff LEFT JOIN users ON users.user_id = hr_staff.staff_id LEFT JOIN role ON users.role_id = role.role_id";
            
			if(count($data)>0)
            {
                $arr =array();
                foreach ($data as $key => $value) {
                    $arr[] = " $key ='$value' ";
                }    
                $sql .= " where ". implode(" and ", $arr);
            }
			$db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        }
        catch(PDOException $e) {
        }
        
        return $result;
    }

    public function all($data=array())
    {
        //Return Variable Array
        $result =array();
        try{
            //Get all Data
            $data = $this->getList();
            $data[0]['pword'] = "********************";
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;

    }
    
    public function add($data)
    {
        $data->pword = password_hash(trim($data->pword),PASSWORD_DEFAULT);
        $result =array();
        try{
            $UserID = $_SESSION['userId'];
            //Insert Query
            $sql ="Insert into users(role_id,pword,active_id,user_id,username,last_modify_by,resetstatus) Values(?,?,?,?,?,?,?)";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->role_id,@$data->pword,@$data->active_id,@$data->user_id,@$data->username,@$UserID,@$data->resetstatus]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Created", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        
        return $result;
    }
    
    public function update($data)
    {
        //$data->pword = isset() ? password_hash(trim($data->pword),PASSWORD_DEFAULT);
        $result =array();
        try{
            $UserID = $_SESSION['userId'];
            //Insert Query
            $sql ="Update users Set role_id=?,pword=?,active_id=?,user_id=?,username=?,last_modify_by=?,resetstatus=? Where id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            //Parameter Placeholder Assigment
            $stmt->execute([@$data->role_id,@$data->pword,@$data->active_id,@$data->user_id,@$data->username,@$UserID,@$data->resetstatus,$data->id]);
            //Get Updated Records
            $data = $this->getList();
            $result = array("status"=> 0, "message"=> "Record Successfully Updated", "data"=>$data); 

            $db = null;
        }
        catch(PDOException $e) {
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }  
        return $result;
    }
    
    public function get($id)
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="Select * from users where id=?";
            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute([$id]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }

    public function listStaffWithNoPass()
    {
        //Return Variable Array
        $result =array();
        try{
            $sql ="SELECT CONCAT(hr_staff.`surname`,' ', hr_staff.`firstname`, ' ', IFNULL(hr_staff.`othername`, '')) AS staffname, 
            hr_staff.staff_id AS user_id, hr_staff.staff_id as id FROM hr_staff WHERE hr_staff.staff_id NOT IN (SELECT users.user_id FROM users)";

            $db = $this->db;
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //Return Variable Assignment (Success)
            $result = array("status"=> 0, "message"=> "Records Retrieved", "data"=>$data); 
            $db = null; //De-assigned Database Variable
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $result = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $result;
    }
    
	
	
	public function validateUser($data)
    {   
        $result =array();
        $returnArr = array();
        try{            
            $sql ="Select users.*, users.user_id as id, concat(surname, ' ', firstname) as staffName from hr_staff left join users on hr_staff.staff_id = users.user_id where username = ?";
            $db = $this->db;
            $stmt = $db->prepare($sql);        
            $stmt->execute([$data->username]);
            $result = $stmt->fetch(PDO::FETCH_OBJ);

			if(password_verify($data->password,$result->pword))
            {
				unset($result->pword);
                if ($result->active_id == '0')
                {
                    $returnArr = array("status"=> 100, "message"=> "Please activate your account using the activation link sent to your email.");
                }
				else if ($result->active_id == '1')
                {
					$returnArr = array("status"=> 0, "message"=> "Login Successful", "data"=> $result);
                }
                else
				{
                    $returnArr = array("status"=> 100, "message"=> "Please activate your account using the activation link sent to your email.");
                }
            }
            else
            {
                $returnArr = array("status"=> 100, "message"=> "Invalid Username or Password");
            }
        }
        catch(PDOException $e) {
            //Return Variable Assignment (Error)
            $returnArr = array("status"=> 100, "message"=> $e->getMessage());
            //Logger    
        }
        return $returnArr;
    }
}
