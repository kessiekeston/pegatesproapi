<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

use Slim\Http\Request;
use Slim\Http\Response;

$app->group("/appsettings", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Appsettings($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Appsettings($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Appsettings($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Appsettings($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/assessmentdetail", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentdetail($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentdetail($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->post("/get-score-by-instruction", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentdetail($this->db);
        $result = $table->StudentByInstructions($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->post("/save-scores", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentdetail($this->db);
        $result = $table->SaveScores($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Assessmentdetail($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Assessmentdetail($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/assessmentgroup", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentgroup($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentgroup($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Assessmentgroup($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Assessmentgroup($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/assessmentinstructions", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentinstructions($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentinstructions($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Assessmentinstructions($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get-assigned-assessment", function(Request $request, Response $response, array $args){
        // $data = json_decode($request->getBody());
            $table = new Assessmentinstructions($this->db);
            $result = $table->LeturesAssigned();
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
        });


    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Assessmentinstructions($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/assessmentsummary", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentsummary($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Assessmentsummary($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Assessmentsummary($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Assessmentsummary($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/class_degree_progcat", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ClassDegreeProgcat($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ClassDegreeProgcat($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ClassDegreeProgcat($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ClassDegreeProgcat($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/classdegree", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Classdegree($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Classdegree($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Classdegree($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Classdegree($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/country", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Country($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Country($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Country($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Country($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/course_registration", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new CourseRegistration($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new CourseRegistration($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new CourseRegistration($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new CourseRegistration($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/coursedeptdetails", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Coursedeptdetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Coursedeptdetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Coursedeptdetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Coursedeptdetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/courses", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Courses($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Courses($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Courses($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Courses($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

}); //->add($auth);

$app->group("/coursestudy", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Coursestudy($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Coursestudy($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Coursestudy($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Coursestudy($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/getbydept/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Coursestudy($this->db);
        $result = $table->getByDept($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/departments", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Departments($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Departments($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Departments($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Departments($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/faculty", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Faculty($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Faculty($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Faculty($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Faculty($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/fdcmapping", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FacDeptCourseMapping($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FacDeptCourseMapping($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new FacDeptCourseMapping($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/fdpcmapping", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FacDeptProgCourseMapping($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new FacDeptProgCourseMapping($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new FacDeptProgCourseMapping($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/gender", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Gender($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Gender($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Gender($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Gender($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/maritalstatus", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new MaritalStatus($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new MaritalStatus($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new MaritalStatus($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new MaritalStatus($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/grades", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Grades($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Grades($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Grades($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Grades($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/hr_banks", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrBanks($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrBanks($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new HrBanks($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new HrBanks($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/hr_department", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrDepartment($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrDepartment($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new HrDepartment($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new HrDepartment($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/getbyfaculty/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new HrDepartment($this->db);
        $result = $table->getByFaculty($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/hr_designation", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrDesignation($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrDesignation($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new HrDesignation($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new HrDesignation($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/hr_employeetype", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrEmployeetype($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrEmployeetype($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new HrEmployeetype($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new HrEmployeetype($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/hr_qualifications", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrQualifications($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrQualifications($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new HrQualifications($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new HrQualifications($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/hr_staff", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrStaff($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrStaff($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new HrStaff($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new HrStaff($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/genstaffcode", function(Request $request, Response $response, array $args){
        $table = new HrStaff($this->db);
        $result = $table->generateStaffCode();
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/hr_staffqualification", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrStaffqualification($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new HrStaffqualification($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new HrStaffqualification($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new HrStaffqualification($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/lectures", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Lectures($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    
    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Lectures($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/get-assigned-lectures", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Lectures($this->db);
        $result = $table->lecturesAssigned(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/list", function(Request $request, Response $response, array $args){
        // $data = json_decode($request->getBody());
            $table = new Lectures($this->db);
            $result = $table->all(array());
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
        });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Lectures($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/lecturercoursemapping", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LecturerCourseMapping($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    
    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LecturerCourseMapping($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/list", function(Request $request, Response $response, array $args){
        // $data = json_decode($request->getBody());
            $table = new LecturerCourseMapping($this->db);
            $result = $table->all(array());
            $status = getStatus($result["status"]);
            $response = $response->withJson($result, $status);
            return $response;
        });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new LecturerCourseMapping($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/levels", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Levels($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Levels($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Levels($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Levels($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/local_govt", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LocalGovt($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LocalGovt($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new LocalGovt($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new LocalGovt($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/getbystate/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new LocalGovt($this->db);
        $result = $table->getLGAByState($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

}); //->add($auth);

$app->group("/login_session", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LoginSession($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new LoginSession($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new LoginSession($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new LoginSession($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/offertype", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Offertype($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Offertype($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Offertype($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Offertype($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/privilegeactivity", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privilegeactivity($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privilegeactivity($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Privilegeactivity($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Privilegeactivity($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/privilegeactivity_details", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeactivityDetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new PrivilegeactivityDetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new PrivilegeactivityDetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new PrivilegeactivityDetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/privilegeclass", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privilegeclass($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privilegeclass($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Privilegeclass($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Privilegeclass($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/privilegedetails", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privilegedetails($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privilegedetails($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Privilegedetails($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Privilegedetails($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/privilegeheader", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privilegeheader($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privilegeheader($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Privilegeheader($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Privilegeheader($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/privileges", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privileges($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Privileges($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Privileges($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Privileges($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/programcategory", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Programcategory($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Programcategory($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Programcategory($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Programcategory($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/programs", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Programs($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Programs($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Programs($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Programs($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/result_mailcron", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ResultMailcron($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new ResultMailcron($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new ResultMailcron($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new ResultMailcron($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/resultpublished", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Resultpublished($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Resultpublished($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Resultpublished($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Resultpublished($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/resultsummary", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Resultsummary($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Resultsummary($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/view-result", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Resultsummary($this->db);
        $result = $table->GenerateResult($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Resultsummary($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Resultsummary($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/role", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Role($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Role($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Role($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Role($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/s_sessionconfig", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new SSessionconfig($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new SSessionconfig($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new SSessionconfig($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new SSessionconfig($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/semester", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Semester($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Semester($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Semester($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Semester($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/sessions", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Sessions($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Sessions($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Sessions($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Sessions($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/state", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new State($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new State($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new State($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new State($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
	
	$app->get("/getbycountry/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new State($this->db);
        $result = $table->getStateByCountry($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/statuses", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Statuses($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Statuses($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Statuses($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Statuses($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/student", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Student($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Student($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Student($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Student($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/genstudentnumber", function(Request $request, Response $response, array $args){
        $table = new Student($this->db);
        $result = $table->generateStaffCode();
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });


})->add($auth);

$app->group("/studentcoursemapping", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new StudentCourseMapping($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new StudentCourseMapping($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new StudentCourseMapping($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new StudentCourseMapping($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/student_status", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new StudentStatus($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new StudentStatus($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new StudentStatus($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new StudentStatus($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/tbl_menu", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TblMenu($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TblMenu($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new TblMenu($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new TblMenu($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/tbl_menuauthorise", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TblMenuauthorise($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new TblMenuauthorise($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new TblMenuauthorise($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new TblMenuauthorise($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->group("/users", function () use ($app) {

    $app->post("/add", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Users($this->db);
        $result = $table->add($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->post("/update", function(Request $request, Response $response, array $args){
        $data = json_decode($request->getBody());
        $table = new Users($this->db);
        $result = $table->update($data);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    $app->get("/list", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Users($this->db);
        $result = $table->all(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

    $app->get("/liststaff", function(Request $request, Response $response, array $args){
    // $data = json_decode($request->getBody());
        $table = new Users($this->db);
        $result = $table->listStaffWithNoPass(array());
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });
    

    $app->get("/get/{id}", function(Request $request, Response $response, array $args){
        $route = $request->getAttribute("route");
        $id = $route->getArgument("id");
        $table = new Users($this->db);
        $result = $table->get($id);
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
        return $response;
    });

})->add($auth);

$app->post("/fileUpload/{folder}", function(Request $request, Response $response, array $args){
		$route = $request->getAttribute("route");
        $folder = $route->getArgument("folder");
        $uploadedFiles = $request->getUploadedFiles();
		$directory = $this->get('upload_directory').'/'.$folder;
        $file = new FileUploads();
        $UploadImgs = $uploadedFiles['UploadImgs'];

        if ($UploadImgs->getError() === UPLOAD_ERR_OK) {
            $result = $file->moveUploadedFile($directory, $UploadImgs);
        }
        $status = getStatus($result["status"]);
        $response = $response->withJson($result, $status);
    return $response;
});

