<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        'db' => [
            'driver'=> 'mysql',
            'host' => '108.61.175.132',
            //'host' => 'DESKTOP-C5STNM8\MSSQL2014',
            'hostVar' => 'host',
            'dbVar' => 'dbname',
            'dbname' => 'pegates_pro',
            'dbuser' => 'root',
            'dbpass' => 'century',
        ],

        'jwt' => [
            'key'=> 'CenturyAppDev123',
            'alg' => array('HS256'),
        ]
    ],
];
